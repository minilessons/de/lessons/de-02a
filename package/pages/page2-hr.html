
<p>Brojeve zapisujemo znamenkama. Pojam <em>brojevni sustav</em> definira način na koji se broj zapisuje uporabom znamenaka. Brojevni sustav može biti <em>pozicijski</em> ali i ne mora.
<em>Pozicijski brojevni sustav</em> je sustav kod kojeg pozicija na kojoj je znamenka zapisana određuje njezinu težinu: sjetite se osnovne škole i jedinica, desetica, stotica, tisućica, i tako dalje.
Vidimo li na papiru zapisano 398, uz pretpostavku da se radi o našem "uobičajenom" dekadskom brojevnom sustavu, reći ćemo da se radi o broju koji ima tri stotice, devet desetica i osam jedinica.</p>

<p>Prije no što nastavimo, ilustrirajmo jedan primjer brojevnog sustava koji nije pozicijski: sjetite se rimskog brojevnog sustava. Broj <span class="lang-constant">IX</span> predstavlja
dekadski broj <span class="lang-constant">9</span>. U tom zapisu <span class="lang-constant">X</span> definitivno nisu jedinice a <span class="lang-constant">I</span> nisu desetice. Spomenuti brojevni
sustav relativnim položajem pojedinih "znamenaka" definira uvećavaju li one ili umanjuju ukupnu vrijednost broja. U prikazanom slučaju, zapis interpretiramo ovako: od broja 10 oduzmi 1 kako bi 
dobio vrijednost zapisanog broja.</p>

<h3>Pozicijski brojevni sustavi</h3>

<p>Pozicijski brojevni sustavi su sustavi kod kojih apsolutni položaj znamenke (gledano od decimalnog zareza) određuje težinu znamenke. Težina je faktor s kojim se znamenka množi (odnosno on određuje
doprinos znamenke vrijednosti zapisanog broja). Baza brojevnog sustava (oznaka B) definira koliko različitih vrijednosti može imati <em>jedna</em> znamenka. Sustav čija je baza B koristi znamenke iz skupa 
{<span class="lang-constant">0</span>, <span class="lang-constant">1</span>, <span class="lang-constant">2</span>, ..., B<span class="lang-constant">-1</span>}. Za brojevne sustave čije su baze do 10 koristimo 
skup uobičajenih znamenaka; za brojevne sustave čija je baza veća od 10, za prikaz znamenaka dodatno koristimo i slova, pa tako nakon znamenke 9 koristimo "znamenku" A, nakon nje "znamenku" B, i tako redom.
</p>

<p>Tablica u nastavku prikazuje neke od češćih baza brojevnih sustava i njihove znamenke.</p>

<table class="mlTableCC">
<caption>Češći brojevni sustavi</caption>
<thead>
<tr><th>Baza</th><th>Naziv</th><th>Znamenke</th></tr>
</thead>
<tbody>
<tr><td>2</td><td>binarni</td><td>{0, 1}</td></tr>
<tr><td>3</td><td>ternarni</td><td>{0, 1, 2}</td></tr>
<tr><td>4</td><td>kvartarni</td><td>{0, 1, 2, 3}</td></tr>
<tr><td>8</td><td>oktalni</td><td>{0, 1, 2, 3, 4, 5, 6, 7}</td></tr>
<tr><td>10</td><td>dekadski</td><td>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}</td></tr>
<tr><td>16</td><td>heksadekadski</td><td>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, a, b, c, d, e, f}</td></tr>
</tbody>
</table>

<p>Pogledajmo jednostavan primjer dekadskog broja <span class="lang-constant">534,73</span>. Cijeli dio broja
možemo razložiti na stotice, desetice i jedinice, a slično možemo učiniti i s decimalnim dijelom. Broj stoga
možemo zapisati na sljedeći način: 
$$ 
\begin{align} 
534,73 &amp;= 5 \cdot 100 + 3 \cdot 10 + 4 \cdot 1 + 7 \cdot \frac{1}{10} + 3 \cdot \frac{1}{100} \\  
&amp;= 5 \cdot 10^2 + 3 \cdot 10^1 + 4 \cdot 10^0 + 7 \cdot 10^{-1} + 3 \cdot 10^{-2}.
\end{align}$$</p>

<p>Gledano od decimalnog zareza na lijevo, prva znamenka (znamenka 4) je na poziciji 0, druga (znamenka 3) na poziciji 1, treća na poziciji 2, i tako dalje.
Gledano od decimalnog zareza na desno, prva znamenka (znamenka 7) je na poziciji -1, druga na poziciji -2, i tako dalje. Doprinos svake
znamenke ukupnoj vrijednosti broja dobiva se kao znamenka pomnožena pripadnim faktorom koji odgovara bazi brojevnog sustava dignutoj
na potenciju koja odgovara poziciji znamenke. Primjerice, najlijevija znamenka (znamenka 5) nalazi se na poziciji 2; pripadni faktor 
je \(10^2\) pa je njezin doprinos vrijednosti broja jednak \(5 \cdot 10^2\), odnosno 5 stotica.</p>

<p>Lijepa stvar je da ovaj zapis vrijedi za proizvoljan pozicijski brojevni sustav s bazom B. Primjerice, razmotrimo
   broj u proizvoljnoj bazi B koji ima <i>n</i> znamenaka u cijelobrojnom dijelu i <i>m</i> znamenaka u decimalnom dijelu. Simbolički,
   takav broj je oblika \(N = a_{n-1} a_{n-2} \ldots a_{2} a_{1} a_{0}, a_{-1} a_{-2} \ldots a_{-m+2} a_{-m+1} a_{-m}\). U skladu 
   s prethodnim primjerom, vrijednost tog broja može se izračunati izrazom:
   $$
     N = \sum\limits_{i=-m}^{n-1} a_i \cdot B^i.
   $$</p>

<p>Prikazani izraz ima vrlo praktičnu primjenu: omogućava nam da jednostavno pretvorimo broj iz proizvoljne baze u dekadski brojevni sustav.
Evo nekoliko primjera. Oznakom N<sub>(B)</sub> označavat ćemo broj zapisan u bazi B; npr. 10100<sub>(2)</sub> je binarni broj
a 10100<sub>(10)</sub> je dekadski broj.</p>


<div class="mlExample"><div class="mlExampleQ"><span class="mlExampleKey">Primjer 1. </span><span class="mlExampleText">Odrediti kako izgleda binarni broj 10100,011<sub>(2)</sub> u dekadskom brojevnom sustavu.</span></div><div class="mlExampleA">
$$
\begin{align}
  10100,011_{(2)} &amp;= 1 \cdot 2^4 + 0 \cdot 2^3 + 1 \cdot 2^2 + 0 \cdot 2^1 + 0 \cdot 2^0 + 0 \cdot 2^{-1} + 1 \cdot 2^{-2} + 1 \cdot 2^{-3} \\
                  &amp;= 1 \cdot 16 + 0 \cdot 8 + 1 \cdot 4 + 0 \cdot 2 + 0 \cdot 1 + 0 \cdot 0,5 + 1 \cdot 0,25 + 1 \cdot 0,125 \\
                  &amp;= 16 + 4 + 0,25 + 0,125 \\
                  &amp;= 20,375
\end{align}
$$
</div></div>

<div class="mlExample"><div class="mlExampleQ"><span class="mlExampleKey">Primjer 2. </span><span class="mlExampleText">Odrediti kako izgleda oktalni broj 371,5<sub>(8)</sub> u dekadskom brojevnom sustavu.</span></div><div class="mlExampleA">
$$
\begin{align}
  371,5_{(8)} &amp;= 3 \cdot 8^2 + 7 \cdot 8^1 + 1 \cdot 8^0 + 5 \cdot 8^{-1} \\
                  &amp;= 3 \cdot 64 + 7 \cdot 8 + 1 \cdot 1 + 5 \cdot 0,125 \\
                  &amp;= 192 + 56 + 1 + 5 \cdot 0,125 \\
                  &amp;= 249,625
\end{align}
$$
</div></div>

<div class="mlExample"><div class="mlExampleQ"><span class="mlExampleKey">Primjer 3. </span><span class="mlExampleText">Odrediti kako izgleda heksadekadski broj 3fd,2e<sub>(16)</sub> u dekadskom brojevnom sustavu.</span></div><div class="mlExampleA">
$$
\begin{align}
  3fd,2e_{(16)} &amp;= 3 \cdot 16^2 + f \cdot 16^1 + d \cdot 16^0 + 2 \cdot 16^{-1} + e \cdot 16^{-2} \\
                  &amp;= 3 \cdot 256 + f \cdot 16 + d \cdot 1 + 2 \cdot 0,0625 + e \cdot 0,00390625 \\
                  &amp;= 3 \cdot 256 + 15 \cdot 16 + 13 \cdot 1 + 2 \cdot 0,0625 + 14 \cdot 0,00390625 \\
                  &amp;= 768 + 240 + 13 + 0,125 + 0,0546875\\
                  &amp;= 1021,1796875
\end{align}
$$
</div></div>

