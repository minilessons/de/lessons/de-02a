
<p>Kroz prethodni dio lekcije savladali smo način pretvorbe brojeva iz proizvoljne baze u dekadsku bazu. Sada ćemo vidjeti kako napraviti suprotan smjer.</p>

<p>Osvrnimo se najprije na važno svojstvo koje ćemo koristiti u postupku pretvorbe. Pogledajmo ponovno dekadski broj 534. Možemo ga raspisati na sljedeći način.$$
\begin{align}
 534 &amp;= 5 \cdot 10^2 + 3 \cdot 10^1 + 4 \\
     &amp;= 10 \cdot (5 \cdot 10^1 + 3 \cdot 10^0) + 4 \\
     &amp;= 10 \cdot 53 + 4. \\
\end{align}
$$
Pogledajte raspis u prvom retku i uočite da uz sve znamenke osim posljednje stoji faktor 10 ili neka njegova viša potencija. U drugom retku faktor 10 je izlučen iz svih članova (osim posljednjeg)
čime je dobiven posljednji redak u kojem je pokazano da se 534 može zapisati kao 10 puta 53 plus 4. Pretpostavimo sada da taj broj želimo cjelobrojno podijeliti s 10:
$$
  \frac{10 \cdot 53 + 4}{10} = \frac{10 \cdot 53}{10} + \frac{4}{10} = 53 + \frac{4}{10}.
$$ 
Rezultat cijelobrojnog dijeljenja je 53 uz ostatak 4. Ostatak pri cjelobrojnom dijeljenju s bazom brojevnog sustava (trenutno je to 10) daje upravo znamenku uz najmanju potenciju baze (uz \(B^0\)). 
Sam rezultat cjelobrojnog djeljenja predstavlja broj bez te znamenke posmaknut za jedno mjesto u desno. I evo nam algoritma za dobiti sve znamenke: na taj rezultat ponovno primijenimo postupak dijeljenja
s bazom: za broj 53 dobit ćemo ostatak 3 i rezultat 5. Konačno, nastavljamo dalje i postupak primijenjujemo na broj 5. Dobit ćemo ostatak 5 i rezultat 0. Kako je rezultat 0, stajemo s postupkom. Ostatci 
koje smo dobivali su redom 4, 3 pa 5 i one odgovaraju znamenkama broja u promatranoj bazi, počev od znamenke uz težinu \(B^0\). Za "uobičajeni" prikaz broja najprije pišemo znamenku koja je uz najveću
potenciju baze; stoga ostatke treba pročitati od zadnjeg prema prvom: broj je 534.
</p>

<p>Možda se pitate zašto smo pokazivali nešto što je očito. Imamo dekadski broj 534; jasno je da su njegove znamenke 5, 3 i 4. Prisjetimo se stoga da se u pozicijskom brojevnom sustavu svaki cijeli
broj može zapisati u prethodno danom obliku sume umnožaka znamenaka i pripadnih težina. Idemo to raspisati samo malo drugačije.
$$
\begin{align}
 N_{(B)} &amp;= \sum \limits_{i=0}^{n-1} a_i \cdot B^i \\
         &amp;= a_0 \cdot B^0 + \sum \limits_{i=1}^{n-1} a_i \cdot B^i \\
         &amp;= a_0 \cdot 1 + B \cdot \sum \limits_{i=1}^{n-1} a_i \cdot B^{i-1} \\
         &amp;= a_0 + B \cdot \sum \limits_{i=1}^{n-1} a_i \cdot B^{i-1}
\end{align}
$$
Što smo ovime dobili? Pokazali smo da se broj \(N_{(B)}\) u bazi B može zapisati kao \(a_0 + B \cdot X\), gdje je \(X = \sum \limits_{i=1}^{n-1} a_i \cdot B^{i-1}\) što je cijeli broj (uvjerite se: u tom 
izrazu nigdje nema dijeljenja). Ako sada napravimo cjelobrojno dijeljenje broja \(N_{(B)}\) s bazom B, kao rezultat ćemo dobiti upravo \(X\) i kao ostatak znamenku najmanje težine \(a_0\). Baš kao u danom
primjeru s dekadskim brojevima, \(X\) predstavlja broj zapisan u bazi B koji je izgubio znamenku najmanje težine i koji je potom posmaknut za jedno mjesto u desno (u zapisu s bazom B). Nad tako dobivenim
brojem potrebno je ponoviti postupak čime ćemo dobiti sljedeću znamenku i novi broj, nad kojim nastavljamo postupak. Ako broj zapisan u bazi B ima <i>n</i> pozicija, postupak će sigurno završiti u <i>n</i> 
koraka, jer svako dijeljenje broj posmiče za jedno mjesto u desno, pa ćemo nakon <i>n</i>-tog dijeljenja stići do broja 0.
</p>

<p>Postupak koji smo opisali naziva se <em>postupak uzastopnog dijeljenja s bazom</em>.</p>

<div class="mlExample"><div class="mlExampleQ"><span class="mlExampleKey">Primjer 1. </span><span class="mlExampleText">Koristeći postupak uzastopnog dijeljenja s bazom prikažite dekadski broj 195 u 
binarnom brojevnom sustavu.</span></div><div class="mlExampleA">Baza brojevnog sustava u koji pretvaramo je B=2. Rješenje:
$$
\begin{array}{rclc}
  197 : 2 &amp; = &amp; 98 &amp; \quad \text{ostatak } 1 \\
   98 : 2 &amp; = &amp; 49 &amp; \quad \text{ostatak } 0 \\
   49 : 2 &amp; = &amp; 24 &amp; \quad \text{ostatak } 1 \\
   24 : 2 &amp; = &amp; 12 &amp; \quad \text{ostatak } 0 \\
   12 : 2 &amp; = &amp; 6 &amp; \quad \text{ostatak } 0 \\
    6 : 2 &amp; = &amp; 3 &amp; \quad \text{ostatak } 0 \\
    3 : 2 &amp; = &amp; 1 &amp; \quad \text{ostatak } 1 \\
    1 : 2 &amp; = &amp; 0 &amp; \quad \text{ostatak } 1 \\
\end{array}
$$
Zadani broj u binarnom brojevnom sustavu glasi <span class="lang-constant">11000101</span>.
</div>
</div>

<p>Pogledajmo još jedan primjer. Ovaj puta neka to bude pretvorba u heksadekadski brojevni sustav.</p>

<div class="mlExample"><div class="mlExampleQ"><span class="mlExampleKey">Primjer 2. </span><span class="mlExampleText">Koristeći postupak uzastopnog dijeljenja s bazom prikažite dekadski broj 453 u 
heksadekadskom brojevnom sustavu.</span></div><div class="mlExampleA">Baza brojevnog sustava u koji pretvaramo je B=16. Rješenje:
$$
\begin{array}{rclc}
  453 : 16 &amp; = &amp; 28 &amp; \quad \text{ostatak } 5 \\
   28 : 16 &amp; = &amp; 1 &amp; \quad \text{ostatak } c \\
    1 : 16 &amp; = &amp; 0 &amp; \quad \text{ostatak } 1 \\
\end{array}
$$
Zadani broj u heksadekadskom brojevnom sustavu glasi <span class="lang-constant">1c5</span>.
</div>
</div>

