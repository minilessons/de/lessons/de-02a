
<p>Ponekad se pretvorba iz jednog brojevnog sustava u drugi može obaviti bitno jednostavnije. Ako su baze oba sustava različite potencije istog broja, možemo koristiti postupak ekspanzije i grupiranja.</p>

<p>Recimo da imamo heksadekadski broj <span class="lang-constant">2f4a</span><sub>(16)</sub> i želimo ga pretvoriti u binarni brojevni sustav. Baza heksadekadskog brojevnog sustava je 16 što je 2<sup>4</sup>. Baza binarnog brojevnog sustava je 2<sup>1</sup>. 
Svaka heksadekadska znamenka stoga će trebati 4/1=4 binarne znamenke. Broj pretvaramo u binarni tako da svaku heksadekadsku znamenku zapišemo kao jednu grupu od četiri binarne znamenke (dakle, radimo <em>ekspanziju</em>). 
U konkretnom slučaju, heksadekadsku znamenku <span class="lang-constant">2</span> zapisat ćemo kao grupu binarnih znamenaka <span class="lang-constant">0010</span>, heksadekadsku znamenku <span class="lang-constant">f</span> 
kao grupu binarnih znamenaka <span class="lang-constant">1111</span>, heksadekadsku znamenku <span class="lang-constant">4</span> kao grupu binarnih znamenaka <span class="lang-constant">0100</span> te
heksadekadsku znamenku <span class="lang-constant">a</span> kao grupu binarnih znamenaka <span class="lang-constant">1010</span>. Odgovarajući binarni broj je <span class="lang-constant">0010111101001010</span>,
pri čemu možemo ukloniti vodeće nule pa dobivamo <span class="lang-constant">10111101001010</span><sub>(2)</sub>. Ovaj postupak vizualiziran je na slici u nastavku.
</p>

<figure class="mlFigure">
<img alt="Pretvorba" src="@#file#(pretvorba1.svg)">
<figcaption>Postupak pretvorbe ekspanzijom.</figcaption>
</figure>

<p>Pretpostavimo sada da trebamo binarni broj pretvoriti u heksadekadski. Omjer znamenaka je i dalje isti: četiri binarne znamenke odgovaraju jednoj heksadekadskoj. Promotrimo binarni broj 
<span class="lang-constant">111001110000010111</span><sub>(2)</sub>. Broj trebamo <em>grupirati</em> u grupe od po četiri bita, ali počev od decimalnog zareza, čime dobivamo
<span class="lang-constant">11 1001 1100 0001 0111</span>. S obzirom da je prva grupa nepotpuna, dopunjavamo je vodećim nulama (jer smo u cjelobrojnom dijelu) pa imamo
<span class="lang-constant">0011 1001 1100 0001 0111</span>. Sada svaku grupu čitamo kao jednu heksadekadsku znamenku, čime dobivamo redom <span class="lang-constant">3</span>,
<span class="lang-constant">9</span>, <span class="lang-constant">c</span>, <span class="lang-constant">1</span> i na kraju <span class="lang-constant">7</span>, odnosno 
heksadekadski broj <span class="lang-constant">39c17</span><sub>(16)</sub>. Ovakva pretvorba ilustrirana je na slici u nastavku.</p>

<figure class="mlFigure">
<img alt="Pretvorba" src="@#file#(pretvorba2.svg)">
<figcaption>Postupak pretvorbe grupiranjem.</figcaption>
</figure>

<p>Kako postupiti u slučajevima poput pretvorbe heksadekadskog broja (primjerice <span class="lang-constant">3f2c5</span><sub>(16)</sub>) u oktalni broj? Baza heksadekadskog brojevnog sustava je 2<sup>4</sup> dok je baza 
oktalnog brojevnog sustava 2<sup>3</sup>. Slijedi da jedna heksadekadska znamenka treba \( 4/3 \approx 1,33\) oktalnih znamenaka za prikaz. U ovakvim slučajevima najprije početni broj možemo ekspanzijom 
zapisati izravno u bazi koja je zajednička za oba sustava (u ovom slučaju to je baza B=2), i potom napraviti grupiranje rezultata u drugu bazu. Heksadekadska znamenka u binarnom prikazu "troši" četiri binarne 
znamenke a oktalna baza tri. Stoga ćemo zadani heksadekadski broj najprije ekspanzijom pretvoriti u binarni: <span class="lang-constant">00111111001011000101</span> pa potom napraviti grupiranje u grupe od
po tri bita <span class="lang-constant">00 111 111 001 011 000 101</span>, izbrisati nepotrebne grupe vodećih nula: <span class="lang-constant">111 111 001 011 000 101</span> i konačno pročitati broj
kao oktalni prevođenjem grupa u oktalne znamenke: <span class="lang-constant">771305</span><sub>(8)</sub>. Postupak je ilustriran na slici u nastavku.</p>

<figure class="mlFigure">
<img alt="Pretvorba" src="@#file#(pretvorba3.svg)">
<figcaption>Postupak pretvorbe ekspanzijom pa grupiranjem.</figcaption>
</figure>

<div class="mlNote">Postupci ekspanzije i grupiranja, u situacijama u kojima su primijenjivi, puno su praktičniji za ručno izvođenje pretvorbi no što su to postupci uzastopnog dijeljenja i množenja. Usvojite
ih - mogu Vam uštedjeti puno vremena.</div>

<p>Osvrnimo se na još jedan praktičan slučaj: pretvorbu dekadskog broja u binarni brojevni sustav, za slučaj kada je dekadski broj relativno malen (primjerice, brojevi od 0 do 255). Vratimo se na osnovni
zapis broja u proizvoljnoj bazi: 
$$
 N_B = \sum \limits_{i=0}^{n-1} a_i \cdot B^i
$$
i konkretizirajmo to za slučaj kada je baza B=2:
$$
 N_2 = \sum \limits_{i=0}^{n-1} a_i \cdot 2^i.
$$
Ako smo u binarnom brojevnom sustavu, svaka od znamenaka može biti ili 0 ili 1; ako je znamenka jednaka 0, pripadna potencija broja 2 ("težina") nema nikakvog utjecaja na vrijednost broja. Ako je znamenka
jednaka 1, tada se broju pridodaje \(a_i \cdot 2^i = 1 \cdot 2^i = 2^i\), odnosno izravno pripadna potencija. Slijedi da je za pretvorbu u binarni brojevni sustav dekadski broj potrebno razložiti na sumu 
potencija broja 2 i potom u binarnom zapisu pišemo 0 na pozicijama potencija koje nisu u toj sumi a 1 inače. Gledamo li prvih 8 potencija broja dva, one su redom (zapamtite ih): 1, 2, 4, 8, 16, 32, 64, 128. 
Idemo sada brzinski pogledati par primjera.
</p>

<p>Dekadski broj 35 u binarnom brojevnom sustavu je:
$$
  35_{(10)} = 32 + 3 = 32 + 2 + 1 = 2^5 + 2^1 + 2^0 = 100011_{(2)}.
$$
Dekadski broj 75 u binarnom brojevnom sustavu je:
$$
  75_{(10)} = 64 + 11 = 64 + 8 + 3 = 64 + 8 + 2 + 1 = 2^6 + 2^3 + 2^1 + 2^0 = 1001011_{(2)}.
$$
Dekadski broj 158 u binarnom brojevnom sustavu je:
$$
  158_{(10)} = 128 + 30 = 128 + 16 + 14 = 128 + 16 + 8 + 6 = 128 + 16 + 8 + 4 + 2 = 2^7 + 2^4 + 2^3 + 2^2 + 2^1 = 10011110_{(2)}.
$$
</p>
